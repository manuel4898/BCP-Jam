using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pausar : MonoBehaviour
{
    public GameObject pausePanel;
    bool paused;
    void Start()
    {
        
    }
    void Update()
    {
        if(!paused && Input.GetKeyDown(KeyCode.P))
        {
            paused = true;
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void Continuar()
    {
        paused = false;
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }
}
