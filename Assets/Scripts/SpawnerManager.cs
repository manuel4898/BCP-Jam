﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject npc;
    public GameObject tempcl;

    public ProductZone zona;

    void Start()
    {
        StartSpawning();
    }

    public void StartSpawning()
    {
        InvokeRepeating("spawnEnemy", 1f, 1f);
    }
    public void StopSpawning()
    {
        CancelInvoke("spawnEnemy");
    }
    private void spawnEnemy()
    {
        if(tempcl==null)
        {
            tempcl = Instantiate(npc, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
            tempcl.GetComponent<Npc>().GenerateID();
            zona.cliente = tempcl.GetComponent<Npc>();
        }
    }


}
