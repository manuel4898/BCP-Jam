using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManoPlayer : MonoBehaviour
{
    Player player;
    GameObject temp;
    public Boxes currentBox;
    private void Start()
    {
        player = transform.parent.GetComponent<Player>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K) && currentBox != null)
        {
            if (!player.holding && player.inventory[currentBox.boxId] > 0)
            {
                temp = Instantiate(currentBox.producto, new Vector3(player.transform.GetChild(0).position.x, player.transform.GetChild(0).position.y, 0), Quaternion.identity);
                temp.GetComponent<Rigidbody2D>().isKinematic = true;
                temp.transform.position = player.transform.GetChild(0).position;
                temp.transform.SetParent(player.transform.GetChild(0));
                player.holding = true;
                player.holdingId = currentBox.boxId;
                player.inventory[currentBox.boxId]--;
            }
            else if (player.holding && player.holdingId == currentBox.producto.GetComponent<Item>().id)
            {
                Destroy(temp);
                player.inventory[currentBox.boxId]++;
                player.holding = false;
                player.holdingId = -1;
            }
            currentBox.GetComponent<SpriteRenderer>().sprite = (player.inventory[currentBox.boxId] <= 0) ? currentBox.spriteVacio : currentBox.spriteStock;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "box")
        {
            currentBox = collision.GetComponent<Boxes>();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "box")
        {
            currentBox = null;
        }
    }
}
