﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int[] inventory;

    public float moveSpeed = 30f;
    public Rigidbody2D rb;


    public bool holding;
    public int holdingId;
    public bool canMove;

    Vector2 movement;
    void Awake()
    {
        inventory = new int[] { 15, 15, 2, 15, 0, 0 };
        moveSpeed = 5f;
        holding = false;
        holdingId = -1;
    }
    void Update()
    {
        MovementPlayer();
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
    void MovementPlayer()
    {
        if (canMove)
        {
            movement.x = Input.GetAxis("Horizontal");
            movement.y = Input.GetAxis("Vertical");
        }
        else
        {
            movement.x = 0;
            movement.y = 0;
        }

        if (movement.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (movement.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

    }
}
