using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Npc : MonoBehaviour
{
    public int id;

    [SerializeField]
    Transform[] waypoints;

    public float moveSpeed = 5f;

    public int wpindex = 0;

    public bool isThere;

    public GameObject client;
    public AsignadorDePedido pedidoHolder;

    public Sprite[] sprites;

    public bool canMove;
    void Start()
    {
        isThere = true;
        canMove = true;
        transform.position = waypoints[wpindex].transform.position;
        this.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
    }

    void Update()
    {
        Move();
    }
    public void Move()
    {
        if (canMove)
        {
            transform.position = Vector2.MoveTowards(transform.position, waypoints[wpindex].transform.position, moveSpeed * Time.deltaTime);
            if (Vector2.Distance(transform.position, waypoints[wpindex].transform.position) < 0.01)
            {
                if (wpindex == 1)
                {
                    canMove = false;
                    Invoke("ResumeWalking", 5f);
                    pedidoHolder.pedido.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = pedidoHolder.sprites[id];
                    pedidoHolder.pedido.SetActive(true);
                }
                wpindex += 1;
            }
        }
        if (wpindex == waypoints.Length)
        {
            Destroy(gameObject);
        }
    }
    public void ResumeWalking()
    {
        canMove = true;
        pedidoHolder.pedido.SetActive(false);
    }
    public bool Fila()
    {
        if(Vector2.Distance(transform.position, client.transform.position) < 2)
        {
            isThere = false;
            return isThere;
        }
        return isThere;
    }
    public void GenerateID()
    {
        id = Random.Range(0, 6);
    }
}
