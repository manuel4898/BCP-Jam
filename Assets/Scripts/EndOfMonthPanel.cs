using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndOfMonthPanel : MonoBehaviour
{
    //Temp---------------------------
    public Player player;
    public SpawnerManager spawner;
    //--------------------------------

    public Bank bank;
    public TimeManager timeManager;

    public Button ventanaBanco;
    public Button ventanaPagos;

    private void OnEnable()
    {
        player.canMove = false;
        spawner.StopSpawning();
        ventanaBanco.onClick.Invoke();
        ventanaPagos.onClick.Invoke();
    }

    public void Finish()
    {
        bank.Finish();
        player.canMove = true;
        spawner.StartSpawning();
        timeManager.StartMonth();
        this.gameObject.SetActive(false);
    }

}
