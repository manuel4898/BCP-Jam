using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductZone : MonoBehaviour
{
    public Npc cliente;
    public Bank banco;
    public Player player;
    public GameObject pedido;

    public AsignadorDePedido pedidoHolder;
    private void Start()
    {
        pedidoHolder.pedido = pedido;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "objects" && cliente!=null)
        {
            if (collision.gameObject.GetComponent<Item>().id == cliente.id)
            {
                BuyItem(collision);
            }
            else
            {
                if (Random.Range(0, 10) < 3) BuyItem(collision);
            }
            cliente.ResumeWalking();
        }
    }

    void BuyItem(Collider2D collision)
    {
        banco.moneyInUser += collision.GetComponent<Item>().precio;

        player.holding = false;
        player.holdingId = -1;
        Destroy(collision.gameObject);
    }
}
