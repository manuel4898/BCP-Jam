using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victoria : MonoBehaviour
{
    private void OnEnable()
    {
        Time.timeScale = 0f;
    } 
    public void ClickContinue()
    {
        Time.timeScale = 1f;
        Destroy(this.gameObject);
    }
    public void ClickMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }
}
