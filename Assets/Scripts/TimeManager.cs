using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeManager : MonoBehaviour
{
    public int day;
    public int month;
    public string dayString;
    public string monthString;
    public TMP_Text dateDisplay;

    public GameObject endOfMonthUI;
    public Bank banco;

    public float cadaCuantoPasaUnDia;
    private void Start()
    {
        cadaCuantoPasaUnDia = 1f;
        day = 1;
        month = 1;
        StartMonth();
    }
    private void Update()
    {
        dayString = (day < 10) ? "0" + day : "" + day;
        monthString = (month < 10) ? "0" + month : "" + month;
        dateDisplay.text = "Mes: " + monthString + " / Dia:" + dayString;  
    }
    public void StartMonth()
    {
        cadaCuantoPasaUnDia = Mathf.Clamp(cadaCuantoPasaUnDia + 1.5f, 2f, 5.5f);
        InvokeRepeating("DayCounter", cadaCuantoPasaUnDia, cadaCuantoPasaUnDia);
    }
    void DayCounter()
    {
        if(day < 30)
        {
            day++;
        }
        else
        {
            CancelInvoke("DayCounter");
            banco.FinDeMes();
            EndMonth();
        }
    }
    void EndMonth()
    {
        day = 1;
        if (month < 12)
            month++;
        else
        {
            month = 1;
            banco.FinAnual();
        }

        endOfMonthUI.SetActive(true);
    }
}
