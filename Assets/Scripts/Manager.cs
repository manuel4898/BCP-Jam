﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Manager : MonoBehaviour
{
    public Button StartButton;
    public string SceneName;

    // Start is called before the first frame update
    void Start()
    {
        StartButton.onClick.AddListener(() => ActivateLoadingCanvas());
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {
        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
    public void IrDirectoAlGameplay()
    {
        SceneName = "GamePlay";
        ActivateLoadingCanvas();
    }
}
