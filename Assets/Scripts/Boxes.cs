using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Boxes : MonoBehaviour
{
    public TMP_Text stockText;
    public GameObject producto;
    public Player player;

    public Sprite spriteStock;
    public Sprite spriteVacio;

    public int boxId;
    private void Start()
    {
        boxId = producto.GetComponent<Item>().id;
    }
    private void Update()
    {
        this.GetComponent<SpriteRenderer>().sprite = (player.inventory[boxId] > 0) ? spriteStock : spriteVacio;
        stockText.text = player.inventory[boxId].ToString();
    }

}
