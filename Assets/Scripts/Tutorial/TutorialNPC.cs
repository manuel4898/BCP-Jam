using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialNPC : MonoBehaviour
{
    Vector2 targetPosition;
    bool inTargetPosition;

    public GameObject continueButton;
    public GameObject tutorialTextBubble;
    public TMP_Text tutorialText;

    public GameObject pedido;
    public TutorialProductZone tutorialProductZone;
    public Manager manager;
    int tutorialStage;
    void Start()
    {
        targetPosition = new Vector2(-1.85f, 2.35f);
    }
    void Update()
    {
        if(!inTargetPosition)
        { 
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, 2f * Time.deltaTime);
            if (Vector2.Distance(transform.position, targetPosition) < 0.01)
            {
                inTargetPosition = true;
                TutorialPart0();
                tutorialTextBubble.SetActive(true);
                continueButton.SetActive(true);
            }
        }
    }
    void TutorialPart0()
    {
        tutorialStage = 0;
        tutorialText.text = "Me alegra que ya estes fuera del hospital. Espero que no hayas perdido tus habilidades de venta.";
    }

    public void ContinueButton()
    {
        switch (tutorialStage)
        {
            case 0:
                {
                    tutorialText.text = "Vamos a hacer unas pruebas. Yo sere tu cliente.";
                    tutorialStage++;
                }
                break;
            case 1:
                {
                    tutorialText.text = "Usa las teclas W A S D para moverte dentro de tu local.";
                    tutorialStage++;
                }
                break;
            case 2:
                {
                    tutorialText.text = "Acercate al estante con botellas de agua. Es el mas lejano. Puedes tomar un producto presionando K.";
                    tutorialStage++;
                }
                break;
            case 3:
                {
                    tutorialText.text = "Recuerda que puedes regresar al mismo estante y presionar nuevamente K para devolver el producto.";
                    tutorialStage++;
                }
                break;
            case 4:
                {
                    tutorialText.text = "Hazlo en caso de que tomes el producto equivocado o el cliente se vaya.";
                    tutorialStage++;
                }
                break;
            case 5:
                {
                    tutorialText.text = "Ahora, vendeme una botella de agua, por favor. Hablar tanto me dio sed.";
                    continueButton.SetActive(false);
                    tutorialProductZone.active = true;
                    tutorialStage++;
                }
                break;
            case 6:
                {
                    tutorialText.text = "Perfecto. Ahora, vendeme una botella de gaseosa dorada.";
                    Invoke("noHayGaseosa", 5f);
                }
                break;
            case 7:
                {
                    tutorialText.text = "Lo olvide. Las dejamos en casa.";
                    tutorialStage++;
                    continueButton.SetActive(true);
                }
                break;
            case 8:
                {
                    tutorialText.text = "No te preocupes. Para tu primer dia, todo estara en su lugar.";
                    tutorialStage++;
                }
                break;
            case 9:
                {
                    tutorialText.text = "De todas formas, si te quedas sin algun producto, intenta ofrecerle algo diferente al cliente. Aveces aceptan.";
                    tutorialStage++;
                }
                break;
            case 10:
                {
                    tutorialText.text = "Recuerda que los fines de mes, debes pagar agua, luz, mercaderia y lo mas importante, tu deuda con el hospital.";
                    tutorialStage++;
                }
                break;
            case 11:
                {
                    tutorialText.text = "No tengas miedo de pedirle ayuda al banco si necesitas dinero adicional. Solo recuerda no exagerar y...";
                    tutorialStage++;
                }
                break;
            case 12:
                {
                    tutorialText.text = "Paga tu deuda! Suerte!";
                    tutorialStage++;
                }
                break;
            case 13:
                {
                    continueButton.SetActive(false);
                    manager.ActivateLoadingCanvas();
                }
                break;
            default:
                break;
        }
    }

    void noHayGaseosa()
    {
        tutorialStage++;
        ContinueButton();
    }
}
