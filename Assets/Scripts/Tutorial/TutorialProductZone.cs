using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialProductZone : MonoBehaviour
{
    public float dinero;
    public bool active;
    public TMP_Text monederoTutorial;
    public GameObject botonContinuar;

    private void Start()
    {
        dinero = 0;
        active = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "objects" && active)
        {
            active = false;
            dinero += 1f;
            monederoTutorial.text = "Dinero: " + dinero.ToString("F2");
            botonContinuar.GetComponent<Button>().onClick.Invoke();
            Destroy(collision.gameObject);
        }
    }
}
