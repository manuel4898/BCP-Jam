using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComprasInversion : MonoBehaviour
{
    public Bank banco;
    public GameObject botonComprarMonster;
    public GameObject estanteMonster;
    public GameObject botonComprarUpgradeMonster;

    public GameObject botonComprarHamburguesa;
    public GameObject estanteHamburguesa;
    public GameObject botonComprarUpgradeHamburguesa;

    public TMP_Text ahorros;
    public void ComprarMonsterUpgrade()
    {
        if (banco.amountSaved >= 15)
        {
            banco.amountSaved -= 15;
            ahorros.text = banco.amountSaved.ToString("F2");
            botonComprarMonster.SetActive(true);
            botonComprarMonster.transform.parent.GetComponent<Image>().color = new Color(147, 255, 117);
            estanteMonster.SetActive(true);
            botonComprarUpgradeMonster.SetActive(false);
        }
    }
    public void ComprarHamburguesaUpgrade()
    {
        if (banco.amountSaved >= 20)
        {
            banco.amountSaved -= 20;
            ahorros.text = banco.amountSaved.ToString("F2");
            botonComprarHamburguesa.SetActive(true);
            botonComprarHamburguesa.transform.parent.GetComponent<Image>().color = new Color(147, 255, 117);
            estanteHamburguesa.SetActive(true);
            botonComprarUpgradeHamburguesa.SetActive(false);
        }
    }
}
