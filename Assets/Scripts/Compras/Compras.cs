using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Compras : MonoBehaviour
{
    public Bank banco;

    public TMP_Text ahorros;
    public Button comprasMayorista;
    private void OnEnable()
    {
        comprasMayorista.onClick.Invoke();
        ahorros.text = banco.amountSaved.ToString();
    }
}
