using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComprasMayorista : MonoBehaviour
{
    public Player player;
    public Bank banco;
    public GameObject[] productos;
    public TMP_Text ahorros;

    private void OnEnable()
    {
        ActualizarUI();
    }
    void ActualizarUI()
    {
        ahorros.text = banco.amountSaved.ToString("F2");
        for (int i = 0; i < productos.Length; i++)
        {
            productos[i].transform.GetChild(2).GetComponent<TMP_Text>().text = "Tienes: " + player.inventory[i];
            productos[i].transform.GetChild(3).GetComponent<TMP_Text>().text = "Precio: " + banco.precioDeProductos[i];
        }
    }
    public void BotonComprar(int id)
    {
        if (banco.ComprarItem(id)) player.inventory[id]++;
        ActualizarUI();
    }
}
