using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BancoPagos : MonoBehaviour
{
    public Bank banco;

    public TMP_Text ahorros;
    public TMP_Text[] serviciosCostosTexto;
    public GameObject[] botonesServicios;

    bool monederoAahorros;
    float inMonedero;
    private void OnEnable()
    {
        ahorros.text = "Ahorros: " + banco.amountSaved.ToString("F2");

        monederoAahorros = banco.moneyInUser > 0.001;
        if (monederoAahorros)
        {
            ahorros.text = "Ahorros: " + banco.amountSaved.ToString("F2");
            inMonedero = banco.moneyInUser;
            InvokeRepeating("PassMoney", 0.1f, 0.1f);
        }

        for (int i = 0; i < botonesServicios.Length; i++)
        {
            botonesServicios[i].SetActive(!banco.sePagoEsteMes[i]);
            botonesServicios[i].GetComponent<Image>().color = Color.white;
        }
        if (banco.esteMesSePidio || banco.sePagoEsteMes[3]) botonesServicios[3].SetActive(false);
        for (int i = 0; i < serviciosCostosTexto.Length; i++)
        {
            serviciosCostosTexto[i].color = (banco.sePagoElMesAnterior[i]) ? Color.black : Color.red;
            serviciosCostosTexto[i].text = (banco.sePagoEsteMes[i]) ? "0.00" : banco.serviciosCostos[i].ToString("F2");
        }
        serviciosCostosTexto[3].color = (banco.sePagoElMesAnterior[3]) ? Color.black : Color.red;
        serviciosCostosTexto[3].text = (banco.esteMesSePidio || banco.sePagoEsteMes[3]) ? "0.00" : banco.montoMensual.ToString("F2");
    }
    public void PagarServicioBoton(int id)
    {
        if (banco.PagarServicio(id))
        {
            botonesServicios[id].SetActive(false);
            serviciosCostosTexto[id].text = "0.00";
            banco.sePagoEsteMes[id] = true;

            ahorros.text = "Ahorros: " + banco.amountSaved.ToString("F2");
        }
        else
        {
            botonesServicios[id].GetComponent<Image>().color = Color.black;
        }
    }

    private void PassMoney()
    {
        banco.amountSaved += 0.05f * inMonedero;
        banco.moneyInUser -= 0.05f * inMonedero;
        ahorros.text = "Ahorros: " + banco.amountSaved.ToString("F2");
        monederoAahorros = banco.moneyInUser > 0.001;

        if (!monederoAahorros)
        {
            banco.amountSaved = Mathf.Round(banco.amountSaved * 100f) / 100f;
            banco.moneyInUser = Mathf.Round(banco.moneyInUser * 100f) / 100f;

            ahorros.text = "Ahorros: " + banco.amountSaved.ToString("F2");

            CancelInvoke("PassMoney");
        }
    }
}
