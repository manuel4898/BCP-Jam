using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BancoPrestamoPagar : MonoBehaviour
{
    public Bank banco;

    public GameObject procesoPago;

    public TMP_Text montoTotal;
    public TMP_Text montoMensual;
    public GameObject botonConfirmar;

    public TMP_Text preViewNuevoMontoAPagar;
    public TMP_Text preViewNuevoAPagarMensual;
    public TMP_Text preViewNuevasCuotas;
    private void OnEnable()
    {
        procesoPago.SetActive(!banco.esteMesSePidio);
        banco.amountIntendedToPay = 0;
        ActualizarDatosUI();
    }

    void ActualizarDatosUI()
    {
        montoTotal.text = banco.montoAPagar.ToString("F2");
        montoMensual.text = "Monto Mensual: " + banco.montoMensual.ToString("F2");
        preViewNuevoMontoAPagar.text = "";
        preViewNuevoAPagarMensual.text = "";
        preViewNuevasCuotas.text = "";
        botonConfirmar.SetActive(false);
    }
    public void PagarPrestamoUI(string value)
    {
        if (float.TryParse(value, out banco.amountIntendedToPay))
        {
            if (banco.amountIntendedToPay > banco.montoAPagar) banco.amountIntendedToPay = banco.montoAPagar;
            preViewNuevoMontoAPagar.text = "Nuevo Total: " + (banco.montoAPagar - banco.amountIntendedToPay).ToString("F2");
            int cuotas_Temp = (banco.amountIntendedToPay >= banco.montoMensual) ? banco.n_cuotasRestantes - 1 : banco.n_cuotasRestantes;
            if (cuotas_Temp < 0) cuotas_Temp = 0;
            preViewNuevoAPagarMensual.text = "Mensual: " + banco.CalcularEnBaseACuotas(banco.montoAPagar - banco.amountIntendedToPay, cuotas_Temp).ToString("F2");
            preViewNuevasCuotas.text = "Cuotas: " + cuotas_Temp;
            botonConfirmar.SetActive(true);
        }
        else
        {
            banco.amountIntendedToPay = 0;
            preViewNuevoMontoAPagar.text = "";
            preViewNuevoAPagarMensual.text = "";
            preViewNuevasCuotas.text = "";
            botonConfirmar.SetActive(false);
        }
    }
    public void ConfirmarPagoPrestamoBoton()
    {
        banco.PagarPrestamo(banco.amountIntendedToPay);
        ActualizarDatosUI();
    }
}
