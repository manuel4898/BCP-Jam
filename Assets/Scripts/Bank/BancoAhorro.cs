using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BancoAhorro : MonoBehaviour
{
    public Bank banco;

    public TMP_Text ahorroCant;
    public TMP_Text ahorroMesAnterior;
    public TMP_Text intereses;
    private void OnEnable()
    {
        ahorroCant.text = banco.amountSaved.ToString("F2");
        ahorroMesAnterior.text = "Mes Anterior: " + banco.amountSavedPrevMonth.ToString("F2");
        intereses.text = "Intereses: " + (Mathf.Round((banco.interestSavings - 1) * 10000f) / 100f).ToString("F2") + "%";
    }
}
