using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BancoPrestamoPrestar : MonoBehaviour
{
    public Bank banco;

    public TMP_Text creditoAprobado;

    public TMP_Text preViewMontoAPagar;
    public TMP_Text preViewMontoAPagarMensual;

    public TMP_Text preViewInterest;

    public GameObject botonConfirmar;
    private void OnEnable()
    {
        ActualizarDatosUI();
    }

    void ActualizarDatosUI()
    {
        creditoAprobado.text = banco.creditLimit.ToString("F2");
        preViewInterest.text = ((banco.interestCredit - 1) * 100f).ToString("F2") + "%";
    }
    public void PedirPrestamoUI(string value)
    {
        if (float.TryParse(value, out banco.amountIntendedToCredit))
        {
            banco.preMontoAPagar = Mathf.Round(banco.amountIntendedToCredit * banco.interestCredit * 100f) / 100f;
            preViewMontoAPagar.text = "Total a pagar: " + banco.preMontoAPagar.ToString("F2");
            banco.preMontoMensual = Mathf.Round(banco.CalcularEnBaseACuotas(banco.preMontoAPagar, banco.n_cuotas) * 100f) / 100f;
            preViewMontoAPagarMensual.text = "Mensual: " + banco.preMontoMensual.ToString("F2");
        }
        else
        {
            banco.amountIntendedToCredit = 0;
            banco.preMontoAPagar = 0;
            banco.preMontoMensual = 0;
            preViewMontoAPagar.text = "Total a pagar: 0.00";
            preViewMontoAPagarMensual.text = "Mensual: 0.00";
        }
    }
    public void AsignarCuotasUI(int _cuotas)
    {
        banco.n_cuotas = _cuotas;
        banco.preMontoMensual = Mathf.Round(banco.CalcularEnBaseACuotas(banco.preMontoAPagar, banco.n_cuotas) * 100f) / 100f;
        preViewMontoAPagarMensual.text = "Mensual: " + banco.preMontoMensual.ToString("F2");
    }
    public void ConfirmarPrestamoBoton()
    {
        banco.PedirPrestamo(banco.amountIntendedToCredit);
    }
}
