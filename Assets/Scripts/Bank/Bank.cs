using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bank : MonoBehaviour
{
    public GameObject DerrotaUI;

    public float moneyInUser;
    public float amountSaved;
    public float amountSavedPrevMonth;
    public float amountSavedPlazo;
    public float amountCredited;
    public float creditLimit;

    
    public float interestSavings;
    public float interestSavingsPlazo;
    public float interestSavingsPlazoAssigned;
    public float interestCredit;
    public float interestCreditAssigned;

    public TMP_Text monedero_UI;


    public GameObject ventanaPrestar;
    public GameObject ventanaPagarPrestamo;

    //Calculo de prestamo

    public float amountIntendedToCredit;
    public float preMontoAPagar;
    public float preMontoMensual;

    public float montoAPagar;
    public float montoMensual;
    public int n_cuotas;
    public int n_cuotasRestantes;

    public bool esteMesSePidio;

    //Calculo de pago restante

    bool sePagoCuota;
    public float amountIntendedToPay;

    //Calculo de pago de servicios

    public float[] serviciosCostosMesAnterior;
    public float[] serviciosCostos;
    public bool[] sePagoElMesAnterior;
    public bool[] sePagoEsteMes;

    //Compras Mayoristas

    public float[] precioMedioDeProductos;
    public float[] precioDeProductos;

    public float cantidadPagadaAlHospital;
    public GameObject victoriaScreen;
    private void Start()
    {
        n_cuotas = 3;

        amountSavedPrevMonth = 0; // placeholder: esto debe ser cambiado por la cantidad promedio que se gana en el primer nivel * 0.9
        creditLimit = 100.001f;
        interestCredit = Random.Range(1.15f, 1.25f);
        interestSavings = Random.Range(1.02f, 1.05f);
        interestSavingsPlazo = Random.Range(1.1f, 1.15f);
        esteMesSePidio = false;
            
        precioMedioDeProductos = new float[] { 1.25f, 1.0f, 2f, 1.25f, 3f, 3.5f };
        precioDeProductos = new float[6];
        serviciosCostosMesAnterior = new float[5];
        serviciosCostos = new float[5];
        sePagoElMesAnterior = new bool[] { true, true, true, true, true };
        sePagoEsteMes = new bool[] { false, false, false, false, false };
    }
    private void Update()
    {
        monedero_UI.text = "Dinero: " + moneyInUser.ToString("F2");
    }

    // Region Pedir Prestamo ----------------------------------------
    public float CalcularEnBaseACuotas(float monto, int _cuotas)
    {
        if (_cuotas <= 0) return 0;
        float montoEnCuotas = Mathf.Round(100f * monto / _cuotas) / 100f;
        return montoEnCuotas;
    }
    public bool PedirPrestamo(float amount)
    {
        bool sePresto = false;
        amount = Mathf.Round(amount * 100f) / 100f;
        if (amount > 0 && amount <= creditLimit)
        {
            amountCredited = amount;
            amountSaved = amountSaved + amountCredited;

            interestCreditAssigned = interestCredit;

            montoAPagar = preMontoAPagar;
            montoMensual = preMontoMensual;
            n_cuotasRestantes = n_cuotas;
            esteMesSePidio = true;
            sePresto = true;
            VerificarDeudaRestante();
        }
        amountIntendedToCredit = 0;
        return sePresto;
    }
    // Fin Region Pedir Prestamo -------------------------------------

    // Region Pagar Prestamo -----------------------------------------
    public void PagarPrestamo(float amount)
    {
        if (amount <= moneyInUser + amountSaved)
        {
            if (amount >= montoMensual && !sePagoCuota)
            {
                PagarCuota();
                amount -= montoMensual;
            }
            else if (!sePagoCuota)
            {
                amountSaved -= amount;
                montoAPagar -= amount;
                montoMensual = CalcularEnBaseACuotas(montoAPagar, n_cuotasRestantes);
            }
            if (sePagoCuota)
            {
                amountSaved -= amount;
                montoAPagar -= amount;
                montoMensual = CalcularEnBaseACuotas(montoAPagar, n_cuotasRestantes);
            }
            VerificarDeudaRestante();
        }
    }
    public bool PagarCuota()
    {
        bool sePago = false;
        if (moneyInUser + amountSaved >= montoMensual && !sePagoCuota)
        {
            if (moneyInUser >= montoMensual) moneyInUser -= montoMensual;
            else
            {
                float tempMonto = montoMensual;
                tempMonto -= moneyInUser;
                amountSaved -= tempMonto;
                moneyInUser = 0;
            }

            montoAPagar -= montoMensual;
            n_cuotasRestantes--;
            sePagoCuota = true;
            sePago = true;
        }
        return sePago;
    }
    public void VerificarDeudaRestante()
    {
        if (montoAPagar < 0.001)
        {
            ventanaPrestar.SetActive(true);
            ventanaPagarPrestamo.SetActive(false);
        }
        else
        {
            ventanaPrestar.SetActive(false);
            ventanaPagarPrestamo.SetActive(true);
        }
    }
    // Fin Region Pagar Prestamo -------------------------------------

    // Region Pago Servicios -----------------------------------------
    void GenerarCostosServicios()
    {
        serviciosCostos[0] = Mathf.Round((Random.Range(4f, 6f) + serviciosCostosMesAnterior[0]) * 100f) / 100f;
        serviciosCostos[1] = Mathf.Round((Random.Range(2f, 3f) + serviciosCostosMesAnterior[1]) * 100f) / 100f;
        serviciosCostos[2] = 10 + serviciosCostosMesAnterior[2];
        serviciosCostos[4] = (victoriaScreen == null) ? 0 : 15 + serviciosCostosMesAnterior[4];

        //aqui puedo hacer un acumulado para verificar si se termino de pagar el hospital, pero es baja prioridad para la 1era etapa de la jam
    }
    public bool PagarServicio(int id)
    {
        bool sePago = false;

        if(id == 3)
        {
            sePago = PagarCuota(); //Modificar esta parte si hago posible pagar cuota durante el mes. Tambien tendr�a que agregar un dehanilitador del boton en la interfaz desde antes de que venga aqu�
        }
        else
        {
            if (moneyInUser + amountSaved >= serviciosCostos[id])
            {
                if (moneyInUser >= serviciosCostos[id]) moneyInUser -= serviciosCostos[id];
                else
                {
                    float tempMonto = serviciosCostos[id];
                    tempMonto -= moneyInUser;
                    amountSaved -= tempMonto;
                    moneyInUser = 0;
                }
                if (id == 4) cantidadPagadaAlHospital += serviciosCostos[4];
                sePago = true;
            }
        }
        amountSaved = Mathf.Round(amountSaved * 100f) / 100f;
        return sePago;
    }

    // Fin Region Pago Servicios -----------------------------------------

    // Region Compra de Items --------------------------------------------
    public void GenerarPreciosMayorista()
    {
        for (int i = 0; i < precioMedioDeProductos.Length; i++)
        {
            precioDeProductos[i] = Mathf.Round(Random.Range(precioMedioDeProductos[i]*0.55f, precioMedioDeProductos[i] * 1.00f) * 100f) / 100f;
        }
    }
    public bool ComprarItem(int id)
    {
        bool seCompro = false;
        if (amountSaved >= precioDeProductos[id])
        {
            amountSaved -= precioDeProductos[id];
            seCompro = true;
        }
        return seCompro;
    }
    // Fin Region Compra de Items --------------------------------------------
    public void FinDeMes()
    {
        amountSaved = amountSaved * interestSavings;
        amountSaved = Mathf.Round(amountSaved * 100f) / 100f;
        creditLimit += 0.1f * (amountSaved - amountSavedPrevMonth);
        VerificarDeudaRestante();
        GenerarCostosServicios();
        GenerarPreciosMayorista();
    }
    public void FinAnual()
    {
        interestCredit = Random.Range(1.18f, 1.28f);
        interestSavings = Random.Range(1.01f, 1.04f);
        interestSavingsPlazo = Random.Range(1.05f, 1.1f);
    }
    public void Finish()
    {
        amountSavedPrevMonth = amountSaved;
        if (sePagoCuota)
        {
            creditLimit += (amountCredited * 0.5f / n_cuotas);
            sePagoCuota = false;
        }
        else if (!esteMesSePidio)
        {
            montoAPagar += Mathf.Round(0.1f * (amountCredited) * 100f) / 100f;
            montoMensual = Mathf.Round(CalcularEnBaseACuotas(montoAPagar, n_cuotasRestantes) * 100f) / 100f;
            creditLimit -= (amountCredited * 0.5f / n_cuotas);
        }
        creditLimit = Mathf.Round(creditLimit * 100f) / 100f;
        VerificarDeudaRestante();

        if (esteMesSePidio) sePagoEsteMes[3] = true;
        sePagoEsteMes[4] = (victoriaScreen == null) || sePagoEsteMes[4];

        for (int i = 0; i < 5; i++)
        {
            if (!sePagoEsteMes[i] && !sePagoElMesAnterior[i]) Derrota();
        }
        for (int i = 0; i < 5; i++)
        {
            sePagoElMesAnterior[i] = sePagoEsteMes[i];
            if (sePagoEsteMes[i]) serviciosCostosMesAnterior[i] = 0;
            else serviciosCostosMesAnterior[i] = serviciosCostos[i]; // Si permito el pago parcial del mes, debo cambiar servicios costos por una nueva variable de solo el mes actual
            sePagoEsteMes[i] = false;
        }
        esteMesSePidio = false;


        if (victoriaScreen != null && cantidadPagadaAlHospital >= 179) victoriaScreen.SetActive(true);
    }
    void Derrota()
    {
        DerrotaUI.SetActive(true);
    }
}
